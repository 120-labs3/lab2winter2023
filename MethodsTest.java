public class MethodsTest{
	
	public static void main(String[] args){
		
		int x = 5;
		System.out.println("From the main method: " + x);
		methodNoInputNoReturn();
		System.out.println("From the main method: " + x);
		
		System.out.println("From the main method: " + x);
		methodOneInputNoReturn(x+10);
		System.out.println("From the main method: " + x);
		
		methodTwoInputNoReturn(3,5.0);
		
		int valueInputInt = methodNoInputReturnInt();
		System.out.println("This is from the methodNoInputReturnInt. The value here is: " + valueInputInt);
		
		double sqrtValue = sumSquareRoot(9,5);
		
		System.out.println("This is the sqrt of 9,5: " + sqrtValue);
		
		String s1 = "java";
		String s2 = "programming";
		
		System.out.println("this it the lenght of the 1st string: " + s1.length());
		
		System.out.println("This is the add one method inside the Second class: " + SecondClass.addOne(50));
		
		SecondClass sc = new SecondClass();
		System.out.println("This is the add two method inside the Second class: " + sc.addTwo(50));
		
		
	}
	
	public static void methodNoInputNoReturn() {
		
		System.out.println("I'm in a method that takes no input and returns nothing");
	
		int x = 20;
		System.out.println("From the methodNoInputNoReturn: " + x);
		
	}
	
	public static void methodOneInputNoReturn(int input){
		
		input = input - 5; 
		System.out.println("Inside the method one input no return " + input);
		
	}
	
	public static void methodTwoInputNoReturn(int inputOne, double inputTwo){
		
		System.out.println("From the methodTwoInputNoReturn: " + inputOne);
		System.out.println("From the methodTwoInputNoReturn: " + inputTwo);
	}
	
	public static int methodNoInputReturnInt(){
		
		return 5;
	}
	public static double sumSquareRoot(int inputOne, int inputTwo){
	
		int sum = inputOne + inputTwo;
		
		return Math.sqrt(sum);
	
	}
	
}