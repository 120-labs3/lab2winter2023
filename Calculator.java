public class Calculator{
	
	public int add(int addInputOne, int addInputTwo){
		
		return addInputOne + addInputTwo;
	}
	
	public int substract(int substractInputOne, int substractInputTwo){
		
		return substractInputOne - substractInputTwo;
		
	}
	
	public static int multiply(int multiplyInputOne, int multiplyInputTwo){
		
		return multiplyInputOne * multiplyInputTwo;
		
	}
	
	public static double divide(double divideInputOne, double divideInputTwo){
		
		return divideInputOne / divideInputTwo;
		
	}
	
	
}