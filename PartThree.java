import java.util.Scanner;

public class PartThree{
	
	public static void main(String[] arg){
	
	Scanner input = new Scanner(System.in);
	Calculator calculate = new Calculator();
	
	System.out.println("Please enter your 1st value: ");
	int Value1 = input.nextInt();
	System.out.println("Please enter your 2nd value: ");
	int Value2 = input.nextInt();
	
	System.out.println("This is the result of the addition: " + calculate.add(Value1,Value2));
	System.out.println("This is the result of the substraction: " + calculate.substract(Value1,Value2));
	System.out.println("This is the result of the multiplication: " + Calculator.multiply(Value1,Value2));
	System.out.println("This is the result of the division: " + Calculator.divide(Value1,Value2));
	
	}
}